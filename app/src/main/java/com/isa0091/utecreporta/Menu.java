package com.isa0091.utecreporta;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class Menu extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
    }


    public void Listado(View view){

        Intent Listado = new Intent(getBaseContext(), Listado.class);
        startActivity(Listado);
    }

    public void CrearReporte(View view){

        Intent Reporte = new Intent(getBaseContext(), Reporte.class);
        startActivity(Reporte);
    }
}
